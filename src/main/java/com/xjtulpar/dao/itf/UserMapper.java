package com.xjtulpar.dao.itf;


import com.xjtulpar.entity.User;

import java.util.List;

public interface UserMapper {
    List<User> queryForList();
    void insertUser(User user);
    User queryUserByPhone(String phone);
}
