package com.xjtulpar.entity;

import java.util.Date;

public class Battery {

    private int id;
    private User batteryUser;
    private String batteryType;
    private String batterySeriesNumber;
    private String factoryName;
    private Date factoryTime;
    private Date saleTime;
    private Date replaceTime;
    private Date changeTime;
    private Date resaleTime;
    private String damageType;
    private String result;
    private boolean isNew;
    private boolean isChanged;
    private boolean isReplaced;
    private boolean isResaled;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getBatteryUser() {
        return batteryUser;
    }

    public void setBatteryUser(User batteryUser) {
        this.batteryUser = batteryUser;
    }

    public String getBatteryType() {
        return batteryType;
    }

    public void setBatteryType(String batteryType) {
        this.batteryType = batteryType;
    }

    public  String getBatterySeriesNumber() {
        return batterySeriesNumber;
    }

    public void setBatterySeriesNumber(String batterySeriesNumber) {
        this.batterySeriesNumber = batterySeriesNumber;
    }

    public String getFactoryName() {
        return factoryName;
    }

    public void setFactoryName(String factoryName) {
        this.factoryName = factoryName;
    }

    public Date getFactoryTime() {
        return factoryTime;
    }

    public void setFactoryTime(Date factoryTime) {
        this.factoryTime = factoryTime;
    }

    public Date getSaleTime() {
        return saleTime;
    }

    public void setSaleTime(Date saleTime) {
        this.saleTime = saleTime;
    }

    public Date getReplaceTime() {
        return replaceTime;
    }

    public void setReplaceTime(Date replaceTime) {
        this.replaceTime = replaceTime;
    }

    public Date getChangeTime() {
        return changeTime;
    }

    public void setChangeTime(Date changeTime) {
        this.changeTime = changeTime;
    }

    public Date getResaleTime() {
        return resaleTime;
    }

    public void setResaleTime(Date resaleTime) {
        this.resaleTime = resaleTime;
    }

    public String getDamageType() {
        return damageType;
    }

    public void setDamageType(String damageType) {
        this.damageType = damageType;
    }

    public String getResult() {
        return result;
    }

    public  void setResult(String result) {
        this.result = result;
    }

    public boolean isNew() {
        return isNew;
    }

    public void setNew(boolean aNew) {
        isNew = aNew;
    }

    public boolean isChanged() {
        return isChanged;
    }

    public void setChanged(boolean changed) {
        isChanged = changed;
    }

    public boolean isReplaced() {
        return isReplaced;
    }

    public void setReplaced(boolean replaced) {
        isReplaced = replaced;
    }

    public boolean isResaled() {
        return isResaled;
    }

    public void setResaled(boolean resaled) {
        isResaled = resaled;
    }
}
