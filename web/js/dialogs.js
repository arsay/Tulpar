/**
 * Created by Aboo on 2017/10/23.
 */


var Dialogs = function () {

    /**
     * UI Root
     * @type {object}
     */
    var ui_main = null;

    /**
     * 返回控件HTML
     * @param title 标题
     * @param message_template 提示内容
     * @param buttons 按钮数组，可以有多个按钮 例如:['Ok', 'Cancel']
     * @returns {string} 控件HTML
     * @private
     */
    function _template(title, message_template, buttons) {
        var html_buttons = '';
        $.each(buttons, function(index, value) {
            html_buttons += _templateButton(value);
        });
        return '\
        \<div alt="整个屏幕的大背景" style="position: fixed; left: 0; top: 0;\
        width: 100%; height: 100%;z-index: 2000;\
        background-color: rgba(0, 0, 0, 0.5);">\
            <div alt="垂直居中div" style="display: -webkit-flex; display: flex;\
            height: 100%;">\
                <div alt="Alert框" class="card" style="width: auto; height: auto; min-height: 250px;\
                margin: auto;\
                background-color: #ffffff;\
                border: none;width: 90%;max-width: 500px;">\
                    <div alt="标题" class="card-header" style="padding: 10px 20px;\
                    font-size:18px; color:#404040; font-weight: bold;"><i id="alert-dialog-icon" class=""></i>\
                    ' + title + '\
                    \</div>\
                    <div alt="内边框" class="card-block">\
                        \<div alt="信息" class="afterIcon" style="padding-top: 10px;margin: auto 25px;\ \
                        font-size:13px;color: #404040;line-height: 32px;min-height: 180px;"><div id="dialog-msg-content" style="">\
                        ' + message_template + '</div>\
                        \</div>\
                    </div>\
                    \<div class="card-footer" alt="按钮组" style="display: -webkit-flex; display: flex;\
                            height: auto;">\
                            ' + html_buttons + '\
                    \</div>\
                </div>\
            </div>\
        </div>';
    }

    /**
     * 返回按钮HTML
     * @param title 按钮标题
     * @returns {string} 按钮HTML
     * @private
     */
    function _templateButton (title) {
        return '\<button href="#" class="btn" name="dialogs_alert_button" style="margin:1px 10px;">\
                ' + title + '\
                \</button>';
    }

    /**
     * 返回response错误信息
     * @param code
     * @param msg
     * @returns {string} 错误信息
     * @private
     */
    function _templateResponseErr(code,msg) {
        return '\<p><strong>خاتالىق نۇمۇرى</strong>  '+code+'</p>\
                <p><strong>خاتالىق ئۇچۇرى</strong>  '+msg+'</p>';
    }

    /**
     * 断言类型
     * @param v 变量
     * @param type 类型
     * @param errorMessage 错误信息
     * @returns {boolean} 断言成功失败
     */
    function assertType(v, type, errorMessage) {
        if (v == null)
            throw new Error(errorMessage);

        if (!(v.constructor+'').match(type))
            throw new Error(errorMessage);
    }

    /**
     * 供外部调用的方法，弹出框
     * @param title
     * @param message_template
     * @param buttons
     * @param iconType icon类型，CoreUI中icon的class，例如:"fa fa-info"
     * @param onSelect
     * @param res  只针对发生错误的情况传response，否则设置为null
     */
    this.alert = function (title, message_template, buttons, iconType, onSelect,res) {

        assertType(title, /String/, 'title 类型 应该是 String');
        assertType(buttons, /Array/, 'buttons 类型 应该是 Array');
        assertType(onSelect, /Function/, 'onSelect 类型 应该是 Function');

        $(document).ready(function() {
            ui_main = $(_template(title, message_template, buttons)).appendTo(document.body);
            if (buttons.length > 0) {
                $('[name="dialogs_alert_button"]').eq(0).addClass('btn-primary');
                if($('[name="dialogs_alert_button"]').eq(1)){$('[name="dialogs_alert_button"]').eq(1).addClass('btn-warning');}
            }
            $('button[name="dialogs_alert_button"]').unbind().bind().click(function () {
                $(this).unbind();
                onSelect($.trim($(this).text()));  //onSelect回调函数
                $(ui_main).remove();
            });
            //设置icon
            if(iconType == '' || iconType == null || iconType == undefined){
                $("#alert-dialog-icon").addClass("fa fa-info");
            }else{
                $("#alert-dialog-icon").addClass(iconType);
            }
            //如果有错误信息，将response.code和response.msg追加到后面
            if(res == '' || res == null || res == undefined || res.code == 0){
                //do nothing
            }else{
                var response_details = _templateResponseErr(res.code,res.msg);
                $("#dialog-msg-content").append(response_details);
            }

        });

    }


};

var dia = new Dialogs();
