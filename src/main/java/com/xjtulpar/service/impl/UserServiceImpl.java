package com.xjtulpar.service.impl;

import com.xjtulpar.dao.itf.UserMapper;
import com.xjtulpar.entity.User;
import com.xjtulpar.service.itf.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    UserMapper userMapper;

    public List<User> getAllUsers() {
        return userMapper.queryForList();
    }

    @Override
    public void insertUser(User user) {
        userMapper.insertUser(user);
    }

    @Override
    public User getUserByPhone(String phone) {
        return userMapper.queryUserByPhone(phone);
    }
}
