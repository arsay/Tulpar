package com.xjtulpar.rest;

import com.xjtulpar.entity.Battery;
import com.xjtulpar.entity.User;
import com.xjtulpar.service.itf.BatteryService;
import com.xjtulpar.service.itf.UserService;
import com.xjtulpar.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
@RequestMapping("battery")
public class BatteryRest {

    @Autowired
    BatteryService batteryService;
    @Autowired
    UserService userService;

    @RequestMapping("all")
    @ResponseBody
    public Object getAllBatterys() {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        resultMap.put("code", 0);
        resultMap.put("msg", "successful");
        resultMap.put("data", batteryService.getAllBatterys());
        return resultMap;
    }

    @RequestMapping(value = "add", method = RequestMethod.POST, consumes = "application/json")
    @ResponseBody
    public Object addNewBatterys(@RequestBody Battery battery) {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        if (battery.getBatteryUser() != null && !StringUtils.isEmpty(battery.getBatteryUser().getUserName())) {
            userService.insertUser(battery.getBatteryUser());
        }
        batteryService.addNewBattery(battery);
        resultMap.put("code", 0);
        resultMap.put("msg", "successful");
        resultMap.put("data", null);
        return resultMap;
    }

    @RequestMapping(value = "update", method = RequestMethod.POST, consumes = "application/json")
    @ResponseBody
    public Object updateBattery(@RequestBody Battery battery) {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        batteryService.updateBattery(battery);
        resultMap.put("code", 0);
        resultMap.put("msg", "successful");
        resultMap.put("data", null);
        return resultMap;
    }

    @RequestMapping(value = "delete", method = RequestMethod.GET)
    @ResponseBody
    public Object deleteBattery(@RequestParam String id) {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        batteryService.deleteBattery(Integer.valueOf(id));
        resultMap.put("code", 0);
        resultMap.put("msg", "successful");
        resultMap.put("data", null);
        return resultMap;
    }

    @RequestMapping(value = "isExist", method = RequestMethod.GET)
    @ResponseBody
    public Object checkExistByPhone(@RequestParam String userPhone) {
        Map<String, Object> resultMap = new HashMap<String, Object>();

        //根据phone数据库查询，返回true / false
        boolean isExist = false;
        User user = userService.getUserByPhone(userPhone);
        if (null != user) {
            isExist = true;
        }

        resultMap.put("code", 0);
        resultMap.put("msg", "successful");
        resultMap.put("data", isExist);
        return resultMap;
    }


}
