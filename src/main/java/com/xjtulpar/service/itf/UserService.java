package com.xjtulpar.service.itf;

import com.xjtulpar.entity.User;
import org.springframework.stereotype.Service;

import java.util.List;

public interface UserService {
    List<User> getAllUsers();
    void insertUser(User user);
    User getUserByPhone(String phone);
}
