package com.xjtulpar.utils;

import org.apache.log4j.Logger;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class FileUploadUtils {
    private static final Logger LOG = Logger.getLogger(FileUploadUtils.class);

    public static void uploadFile(MultipartFile multipartFile, String targetDir, String fileName) {
        File fileDir = new File(targetDir);
        if (!fileDir.exists()) {
            fileDir.mkdirs();
        }
        try (OutputStream stream = new FileOutputStream(targetDir + File.separator + fileName)) {
            stream.write(multipartFile.getBytes());
        } catch (IOException ex) {
            LOG.error("upload file and write server error", ex);
        }
    }
}
