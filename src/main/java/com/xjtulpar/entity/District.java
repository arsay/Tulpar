package com.xjtulpar.entity;

public class District {
    private int id;
    private City city;
    private String ugName;
    private String chName;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public String getUgName() {
        return ugName;
    }

    public void setUgName(String ugName) {
        this.ugName = ugName;
    }

    public String getChName() {
        return chName;
    }

    public void setChName(String chName) {
        this.chName = chName;
    }
}
