package com.xjtulpar.service.itf;

import com.xjtulpar.entity.Region;

import java.util.List;

public interface RegionService {
    List<Region> getCities();
    List<Region> getDistrictsByCityId(int cityId);
}
