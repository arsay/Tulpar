package com.xjtulpar.dao.itf;

import com.xjtulpar.entity.Battery;

import java.util.List;

public interface BatteryMapper {
    public List<Battery> queryAllBatterys();
    public void insertNewBattery(Battery battery);
    public void updateBattery(Battery battery);
    public void deleteBattery(int id);
}
