package com.xjtulpar.service.itf;

import com.xjtulpar.entity.Manager;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * Created by Levi on 2017/10/10.
 */
public interface ManagerService {

    List<Manager> getAllManagers();

    Manager getById(int mId);

    public void addManager(Manager manager);

    public void updateManager(Manager manager);

    public void deleteManager(int mId);

    void updateIcon(MultipartFile[] files,String mId);
}
