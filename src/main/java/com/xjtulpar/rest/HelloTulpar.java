package com.xjtulpar.rest;

import com.xjtulpar.entity.User;
import com.xjtulpar.service.itf.UserService;
import com.xjtulpar.utils.FileUploadUtils;
import com.xjtulpar.utils.PathUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.Map;

@Controller
public class HelloTulpar {
    private static final Logger log = Logger.getLogger(HelloTulpar.class);
    @Autowired
    UserService service;

    @RequestMapping("test")
    @ResponseBody
    public Object test(@RequestParam String name) {
        log.info("begin rest!");
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("data",service.getAllUsers());

        return map;
    }

    @RequestMapping(value = "u", method = RequestMethod.POST, consumes = "multipart/form-data")
    @ResponseBody
    public Object uploadFile(@RequestParam(value = "file") MultipartFile[] files) {
        String targerDir = PathUtils.getRealPath("/img/avatars");
        FileUploadUtils.uploadFile(files[0], targerDir,files[0].getOriginalFilename());
        return "success";
    }
}
