package com.xjtulpar.utils;

import org.apache.log4j.Logger;
import org.springframework.util.StringUtils;
import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.ServletContext;

public class PathUtils {
    private static final Logger LOG = Logger.getLogger(PathUtils.class);

    private static WebApplicationContext applicationContext = ContextLoader.getCurrentWebApplicationContext();
    private static ServletContext context = applicationContext.getServletContext();

    public static String getRealPath(String dirName) {
        if (StringUtils.isEmpty(dirName)) {
            LOG.error("invalid directory name, please check the directory name!");
            return null;
        }
        return context.getRealPath(dirName);
    }
}
