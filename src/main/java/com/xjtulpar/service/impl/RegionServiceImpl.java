package com.xjtulpar.service.impl;

import com.xjtulpar.dao.itf.RegionMapper;
import com.xjtulpar.entity.Region;
import com.xjtulpar.service.itf.RegionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class RegionServiceImpl implements RegionService {
    @Autowired
    RegionMapper regionMapper;
    public List<Region> getCities() {
        return regionMapper.queryCities();
    }

    public List<Region> getDistrictsByCityId(int cityId) {
        return regionMapper.queryDistrictsByCityId(cityId);
    }
}
