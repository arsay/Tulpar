/*
SQLyog Ultimate v12.08 (64 bit)
MySQL - 5.7.20-log : Database - tulparerp
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`tulparerp` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `tulparerp`;

/*Table structure for table `battery` */

DROP TABLE IF EXISTS `battery`;

CREATE TABLE `battery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `batterytype` varchar(20) NOT NULL,
  `batteryseriesnumber` varchar(40) NOT NULL,
  `factoryname` varchar(50) NOT NULL,
  `factorytime` date DEFAULT NULL,
  `saletime` date DEFAULT NULL,
  `replacetime` date DEFAULT NULL,
  `changetime` date DEFAULT NULL,
  `resaletime` date DEFAULT NULL,
  `damagetype` varchar(40) NOT NULL,
  `result` varchar(40) NOT NULL,
  `isnew` tinyint(1) DEFAULT NULL,
  `ischanged` tinyint(1) DEFAULT NULL,
  `isreplaced` tinyint(1) DEFAULT NULL,
  `isresaled` tinyint(1) DEFAULT NULL,
  `buphone` varchar(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_mid_battery` (`buphone`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `battery` */

insert  into `battery`(`id`,`batterytype`,`batteryseriesnumber`,`factoryname`,`factorytime`,`saletime`,`replacetime`,`changetime`,`resaletime`,`damagetype`,`result`,`isnew`,`ischanged`,`isreplaced`,`isresaled`,`buphone`) values (1,'123','123456','tulpar','2017-10-14','2017-10-14','2017-10-14','2017-10-14','2017-10-14','buzulgan','sss',0,0,0,0,'18999888348'),(2,'123','123456','tulpar','2017-09-10','2017-09-10','2017-09-10','2017-09-10','2017-09-10','buzulgan','sss',0,0,0,0,'18999888348'),(3,'1234','7897','xj','2017-10-20','2017-10-03','2017-10-20','2017-10-18','2017-10-11','exit','wo',1,1,1,0,'18999888348'),(4,'6040','6543221','sampul','2017-10-14','2017-10-14','2017-10-14','2017-10-14','2017-10-14','buzulgan','sss',0,0,0,0,'18999888349'),(5,'6040','6543221','sampul','2017-10-14','2017-10-14','2017-10-14','2017-10-14','2017-10-14','buzulgan','sss',0,0,0,0,'18999888349');

/*Table structure for table `city` */

DROP TABLE IF EXISTS `city`;

CREATE TABLE `city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `chname` varchar(30) DEFAULT NULL,
  `ugname` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=336 DEFAULT CHARSET=utf8;

/*Data for the table `city` */

insert  into `city`(`id`,`chname`,`ugname`) values (332,'阿克苏地区','ئاقسۇ ۋىلايىتى'),(334,'喀什地区','قەشقەر ۋىلايىتى'),(335,'和田地区','خوتەن ۋىلايىتى');

/*Table structure for table `district` */

DROP TABLE IF EXISTS `district`;

CREATE TABLE `district` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `chname` varchar(80) DEFAULT NULL,
  `ugname` varchar(80) DEFAULT NULL,
  `cityid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2839 DEFAULT CHARSET=utf8;

/*Data for the table `district` */

insert  into `district`(`id`,`chname`,`ugname`,`cityid`) values (2806,'阿克苏市','ئاقسۇ شەھىرى',332),(2807,'温宿县','ئونسۇ ناھىيەسى',332),(2808,'库车县','كۇچا ناھىيەسى',332),(2809,'沙雅县','شايار ناھىيەسى',332),(2810,'新和县','توقسۇ ناھىيەسى',332),(2811,'拜城县','باي ناھىيەسى',332),(2812,'乌什县','ئۈچ تۇرپان',332),(2813,'阿瓦提县','ئاۋات ناھىيەسى',332),(2814,'柯坪县','كەلپىن ناھىيەسى',332),(2819,'喀什市','قەشقەر شەھىرى',334),(2820,'疏附县','كوناشەھەر ناھىيەسى',334),(2821,'疏勒县','يىڭىشەھەر ناھىيەسى',334),(2822,'英吉沙县','يىڭىسار ناھىيەسى',334),(2823,'泽普县','پوسكام ناھىيەسى',334),(2824,'莎车县','يەكەن ناھىيەسى',334),(2825,'叶城县','قاغىلىق ناھىيەسى',334),(2826,'麦盖提县','مەكىت ناھىيەسى',334),(2827,'岳普湖县','يوپۇرغا ناھىيەسى',334),(2828,'伽师县','پەيزىۋات ناھىيەسى',334),(2829,'巴楚县','مارالبېشى ناھىيەسى',334),(2830,'塔什库尔干塔吉克自治县','تاشقورغان تاجىك ئاپتۇنۇم ناھىيسى',334),(2831,'和田市','خوتەن شەھىرى',335),(2832,'和田县','خوتەن ناھىيەسى',335),(2833,'墨玉县','قارىقاش ناھىيەسى',335),(2834,'皮山县','گۇما ناھىيەسى',335),(2835,'洛浦县','لوپ ناھىيەسى',335),(2836,'策勒县','چىرا ناھىيەسى',335),(2837,'于田县','كىرىيە ناھىيەسى',335),(2838,'民丰县','نىيە ناھىيەسى',335);

/*Table structure for table `location` */

DROP TABLE IF EXISTS `location`;

CREATE TABLE `location` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `districtId` int(11) DEFAULT NULL,
  `village` varchar(120) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `location` */

insert  into `location`(`id`,`districtId`,`village`) values (1,2806,'sampul');

/*Table structure for table `manager` */

DROP TABLE IF EXISTS `manager`;

CREATE TABLE `manager` (
  `mid` int(11) NOT NULL AUTO_INCREMENT,
  `mname` varchar(60) NOT NULL,
  `mtel` varchar(20) NOT NULL,
  `memail` varchar(30) DEFAULT NULL,
  `mpwd` varchar(20) DEFAULT '123456',
  `mrole` varchar(30) NOT NULL,
  `mdistrictid` int(11) NOT NULL,
  `mpic` text,
  `mvillage` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`mid`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

/*Data for the table `manager` */

insert  into `manager`(`mid`,`mname`,`mtel`,`memail`,`mpwd`,`mrole`,`mdistrictid`,`mpic`,`mvillage`) values (1,'ئالىم','12345678900','alim@mail.com','123456','root',2806,'/img/avatars/default.jpg',NULL),(2,'بارات','243545768766','barat@mail.com','123456','customer',2806,'/img/avatars/default.jpg',NULL),(3,'غالىب','243545768766','barat@mail.com','123456','customer',2806,'/img/avatars/default.jpg',NULL),(4,'غالىب','243545768766','barat@mail.com','123456','customer',2806,'/img/avatars/default.jpg',NULL),(5,'ئابدۇمىجىت','18999888348','toghrak@foxmail.com',NULL,'customer',2806,'img/avatars/default.jpg',NULL),(6,'man','12345678900','alim@mail.com','123456','root',2806,'img/avatars/default.jpg',NULL),(7,'san','12345678900','alim@mail.com','123456','root',2806,'img/avatars/default.jpg',NULL),(8,'man','12345678900','alim@mail.com','123456','root',2807,'img/avatars/default.jpg',NULL),(9,'ھەسەن','12345678900','alim@mail.com','123456','root',2808,'img/avatars/default.jpg',NULL);

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uname` varchar(60) DEFAULT NULL,
  `uphone` varchar(11) NOT NULL,
  `udistrictid` int(11) DEFAULT NULL,
  `uvillage` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uphone` (`uphone`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `user` */

insert  into `user`(`id`,`uname`,`uphone`,`udistrictid`,`uvillage`) values (1,'hasan','18999888348',2807,'yok'),(2,'hasan','18999888349',2807,'yok');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
