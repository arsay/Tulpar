package com.xjtulpar.rest;

import com.xjtulpar.entity.Manager;
import com.xjtulpar.service.itf.ManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Levi on 2017/10/10.
 */
@Controller
@RequestMapping("manager")
public class ManagerRest {

    @Autowired
    ManagerService managerService;

    @RequestMapping("all")
    @ResponseBody
    public Object getAll(){
        Map<String, Object> resultMap = new HashMap<String, Object>();
        resultMap.put("code",0);
        resultMap.put("msg","successful");
        resultMap.put("data",managerService.getAllManagers());
        return resultMap;
    }

    @RequestMapping("one")
    @ResponseBody
    public Object getById(@RequestParam int mId){
        Map<String, Object> resultMap = new HashMap<String, Object>();
        resultMap.put("code",0);
        resultMap.put("msg","successful");
        resultMap.put("data",managerService.getById(mId));
        return resultMap;
    }

    @RequestMapping(value = "add",method = RequestMethod.POST,consumes = "application/json")
    @ResponseBody
    public Object addManager(@RequestBody Manager manager)  {
        Map<String,Object> resultMap = new HashMap<String,Object>();
        managerService.addManager(manager);
        resultMap.put("code",0);
        resultMap.put("msg","successful");
        resultMap.put("data",null);
        return resultMap;
    }

    @RequestMapping(value = "update",method = RequestMethod.POST,consumes = "application/json")
    @ResponseBody
    public Object updateManager(@RequestBody Manager manager) {
        Map<String,Object> resultMap = new HashMap<String,Object>();
        managerService.updateManager(manager);
        resultMap.put("code",0);
        resultMap.put("msg","successful");
        resultMap.put("data",null);
        return resultMap;
    }

    @RequestMapping(value = "delete",method = RequestMethod.GET)
    @ResponseBody
    public Object deleteManager(@RequestParam String mId) {
        Map<String,Object> resultMap = new HashMap<String,Object>();
        managerService.deleteManager(Integer.valueOf(mId));
        resultMap.put("code",0);
        resultMap.put("msg","successful");
        resultMap.put("data",null);
        return resultMap;
    }

    @RequestMapping(value = "upload", method = RequestMethod.POST, consumes = "multipart/form-data")
    @ResponseBody
    public Object updateIcon(@RequestParam(value = "file") MultipartFile[] files,@RequestParam String mId){
        Map<String,Object> resultMap = new HashMap<String,Object>();
        managerService.updateIcon(files , mId);
        resultMap.put("code",0);
        resultMap.put("msg","successful");
        resultMap.put("data",null);
        return resultMap;
    }
}
