package com.xjtulpar.entity;

/**
 * Created by Levi on 2017/10/10.
 */
public class Manager {

    private int mId;
    private String mPic;
    private String mName;
    private String mEmail;
    private String mTel;
    private String mPwd;
    private String mRole;
    private String mVillage;
    private District mDistrict;

    public int getmId() {
        return mId;
    }

    public void setmId(int mId) {
        this.mId = mId;
    }

    public String getmPic() {
        return mPic;
    }

    public void setmPic(String mPic) {
        this.mPic = mPic;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public String getmEmail() {
        return mEmail;
    }

    public void setmEmail(String mEmail) {
        this.mEmail = mEmail;
    }

    public String getmPwd() {
        return mPwd;
    }

    public void setmPwd(String mPwd) {
        this.mPwd = mPwd;
    }

    public String getmRole() {
        return mRole;
    }

    public void setmRole(String mRole) {
        this.mRole = mRole;
    }

    public String getmVillage() {
        return mVillage;
    }

    public void setmVillage(String mVillage) {
        this.mVillage = mVillage;
    }

    public String getmTel() {
        return mTel;
    }

    public void setmTel(String mTel) {
        this.mTel = mTel;
    }

    public District getmDistrict() {
        return mDistrict;
    }

    public void setmDistrict(District mDistrict) {
        this.mDistrict = mDistrict;
    }
}
