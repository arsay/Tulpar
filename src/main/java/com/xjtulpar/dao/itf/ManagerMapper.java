package com.xjtulpar.dao.itf;

import com.xjtulpar.entity.Manager;

import java.util.List;
import java.util.Map;

/**
 * Created by Levi on 2017/10/10.
 */
public interface ManagerMapper {
    List<Manager> queryAllManagers();

    Manager queryById(int mId);

    void insertManager(Manager manager);

    void updateManager(Manager manager);

    void updateIcon(Manager manager);

    void deleteManager(int mId);

}
