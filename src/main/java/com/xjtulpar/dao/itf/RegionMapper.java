package com.xjtulpar.dao.itf;

import com.xjtulpar.entity.Region;

import java.util.List;

public interface RegionMapper {
    List<Region> queryCities();
    List<Region> queryDistrictsByCityId(int cityId);
}
