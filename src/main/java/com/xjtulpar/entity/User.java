package com.xjtulpar.entity;

public class User {

    private int userId;
    private String userName;
    private String userPhone;
    private String userVillage;
    private District district;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int id) {
        this.userId = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String username) {
        this.userName = username;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String phone) {
        this.userPhone = phone;
    }

    public String getUserVillage() {
        return userVillage;
    }

    public void setUserVillage(String userVillage) {
        this.userVillage = userVillage;
    }

    public District getDistrict() {
        return district;
    }

    public void setDistrict(District district) {
        this.district = district;
    }
}
