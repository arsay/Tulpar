package com.xjtulpar.rest;

import com.xjtulpar.entity.Region;
import com.xjtulpar.service.itf.RegionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("region")
public class RegionRest {
    @Autowired
    RegionService regionService;

    @ResponseBody
    @RequestMapping("cities")
    public Object getCities() {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        List<Region> cities = regionService.getCities();
        if (cities == null || cities.isEmpty()) {
            resultMap.put("code",1);
            resultMap.put("msg","error");
            resultMap.put("data",null);
        } else {
            resultMap.put("code", 0);
            resultMap.put("msg","successful");
            resultMap.put("data", cities);
        }
        return resultMap;
    }

    @ResponseBody
    @RequestMapping("districts")
    public Object getDistrictsByCityId(@RequestParam String cityId) {
        Map<String,Object> resultMap = new HashMap<String,Object>();
        if(cityId == null){
            resultMap.put("code",1);
            resultMap.put("msg","error");
            resultMap.put("data",null);
            return resultMap;
        }
        List<Region> districts = regionService.getDistrictsByCityId(Integer.valueOf(cityId));
        if (districts == null || districts.isEmpty()) {
            resultMap.put("code",1);
            resultMap.put("msg", "error");
            resultMap.put("data",null);
        } else {
            resultMap.put("code", 0);
            resultMap.put("msg","successful");
            resultMap.put("data", districts);
        }
        return resultMap;
    }
}
