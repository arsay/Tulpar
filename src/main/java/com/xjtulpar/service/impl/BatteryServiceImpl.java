package com.xjtulpar.service.impl;

import com.xjtulpar.dao.itf.BatteryMapper;
import com.xjtulpar.entity.Battery;
import com.xjtulpar.service.itf.BatteryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class BatteryServiceImpl implements BatteryService {
    @Autowired
    BatteryMapper batteryMapper;
    public List<Battery> getAllBatterys() {
        return batteryMapper.queryAllBatterys();
    }
    public void addNewBattery(Battery battery){
        batteryMapper.insertNewBattery(battery );
    }
    public void updateBattery(Battery battery) {
        batteryMapper.updateBattery(battery);
    }
    public void deleteBattery(int id) {
        batteryMapper.deleteBattery(id);
    }

}
