package com.xjtulpar.service.itf;

import com.xjtulpar.entity.Battery;

import java.util.List;

public interface BatteryService {
    List<Battery> getAllBatterys();
    void addNewBattery(Battery battery );
    void updateBattery(Battery battery);
    void deleteBattery(int id);
}
